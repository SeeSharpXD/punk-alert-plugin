﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot;
using System.ComponentModel.Composition;
using PunkAlertPlugin.GUI;
using PunkAlertPlugin.Settings;
using PunkAlertPlugin.MoreMethods;
using System.Timers;
using ZzukBot.Settings;
using ZzukBot.Game.Statics;
using System.Media;
using System.IO;
using ZzukBot.Objects;
using ZzukBot.Constants;

namespace PunkAlertPlugin
{
    [Export(typeof(ZzukBot.ExtensionFramework.Interfaces.IPlugin))]
    public class PunkAlertPlugin : ZzukBot.ExtensionFramework.Interfaces.IPlugin
    {
        public PluginGUI pluginUI;
        public PunkAlertSettings settingsPlugin = new PunkAlertSettings();
        public Timer tickTimer;
        public Timer clearIgnoreTimer;
        public SoundPlayer alertSound;
        public List<Stream> soundFile;
        public List<WoWUnit> ignoredPlayers;
        public bool isStarted;
        public string Author => "SeeSharp";

        public string Name => "Punk Alert (Beta)";

        public int Version => 4;

        public bool Load()
        {
            try
            {
                settingsPlugin = OptionManager.Get("PunkAlert").LoadFromJson<PunkAlertSettings>();
                //Helpers.PrintToChat("Loaded");
                //settingsCC = OptionManager.Get(shamanConstanctsObj.ccName).LoadFromJson<CCSettings>();
            }
            catch (Exception exc)
            {
                //settingsCC = new CCSettings();
                //settingsCC.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Default);
                //Helpers.PrintToChat("Error Loading Plugin Settings " + exc.GetType().ToString());
                System.Windows.Forms.MessageBox.Show("Error Loading PunkAlert!");
            }
            try
            {
                //check for complete object. If object is incomplete, create a new one
                if (settingsPlugin.reconnectIfDC)
                {
                    settingsPlugin.reconnectIfDC = settingsPlugin.reconnectIfDC;
                }
            }
            catch (Exception ex)
            {
                settingsPlugin = new PunkAlertSettings();
                OptionManager.Get("PunkAlert").SaveToJson(settingsPlugin);
                //Helpers.PrintToChat("JSON File Created!");
            }
            /*
            if (ObjectManager.Instance.IsIngame)
            {
                Helpers.PrintToChat("Enabled!");
            }
            */
            pluginUI = new PluginGUI();
            tickTimer = new Timer();
            clearIgnoreTimer = new Timer();
            alertSound = new SoundPlayer();
            soundFile = new List<Stream>();
            ignoredPlayers = new List<WoWUnit>();
            soundFile.Add(Properties.Resources.PVPSoundA);
            soundFile.Add(Properties.Resources.PvPSoundB);
            soundFile.Add(Properties.Resources.PvPSoundC);
            tickTimer.Interval = 10000;
            tickTimer.Elapsed += new ElapsedEventHandler(OnTimerTick);
            clearIgnoreTimer.Interval = 60 * 1000;
            clearIgnoreTimer.Elapsed += new ElapsedEventHandler(clearIgnoredPlayers);
            isStarted = false;
            if (settingsPlugin.scanMSSpeed < 1000)
            {
                settingsPlugin.scanMSSpeed = 1000;
            }
            tickTimer.Start();
            clearIgnoreTimer.Start();
            return true;
        }

        public void ShowGui()
        {
            pluginUI.Dispose();
            pluginUI = new PluginGUI();
            pluginUI.parentPluginObj = this;
            pluginUI.LoadSettingsToForm();
            pluginUI.Show();
        }

        public void Unload()
        {
            _Dispose();
        }

        public void _Dispose()
        {
            try
            {
                pluginUI.Dispose();
                settingsPlugin = null;
                tickTimer.Dispose();
                clearIgnoreTimer.Dispose();
                alertSound.Dispose();
                soundFile = null;
                ignoredPlayers = null;
                if (!ObjectManager.Instance.IsIngame)
                {
                    //
                }
                else
                {
                    Helpers.PrintToChat("Disabled");
                }

            }
            catch (Exception exc)
            {
                //Helpers.PrintToChat("Error! " + exc.GetType().ToString());
            }
        }

        public void PlayAlert(string alertMessage = "", int alertType = 0, bool useAudio = true, int audioToUse = 0)
        {
            try
            {
                if (useAudio)
                {
                    soundFile[audioToUse].Position = 0;
                    alertSound.Stream = soundFile[audioToUse];
                    alertSound.Stream.Position = 0;
                    alertSound.Play();
                }
                if (alertType == 0)
                {
                    Helpers.PrintToChat(alertMessage);
                }
                else if (alertType == 1)
                {
                    Helpers.PrintToChat(alertMessage);
                }
            }
            catch(Exception exc)
            {
                
            }
        }
        void OnTimerTick(object sender, ElapsedEventArgs e)
        {

            try
            {
                if (!ObjectManager.Instance.IsIngame)
                {
                    tickTimer.Interval = 10000;
                    return;
                }
                else
                {
                    List<WoWUnit> tempNearUnits;
                    List<WoWUnit> tempAttackers;
                    tempNearUnits = ObjectManager.Instance.Players;
                    tempAttackers = UnitInfo.Instance.NpcAttackers;
                    if (tickTimer.Interval != settingsPlugin.scanMSSpeed)
                    {
                        tickTimer.Interval = settingsPlugin.scanMSSpeed;
                        Helpers.PrintToChat("Started - Scanning for Possible Ganks every " + settingsPlugin.scanMSSpeed.ToString() + "MS!");
                        isStarted = true;
                    }
                    if (settingsPlugin.alertStyle == 0)
                    {
                        if (tempAttackers.Count > 0)
                        {
                            for (int i = 0; i < tempAttackers.Count; i += 1)
                            {
                                if (tempAttackers[i].IsPlayer && !ignoredPlayers.Contains(tempAttackers[i]) && !settingsPlugin.ignoredPlayers.Contains(tempAttackers[i].Name))
                                {
                                    PlayAlert("ALERT! " + tempAttackers[i].Name + " - " + tempAttackers[i].Level.ToString(), settingsPlugin.alertType, settingsPlugin.playAudio, settingsPlugin.audioToPlay);
                                    ignoredPlayers.Add(tempAttackers[i]);
                                }
                            }
                        }
                    }
                    else
                    {
                        //Helpers.PrintToChat("TICK " + tickTimer.Interval.ToString());
                        for (int i = 0; i < tempNearUnits.Count; i += 1)
                        {
                            if (tempNearUnits[i].Reaction == Enums.UnitReaction.Hostile && ignoredPlayers.Contains(tempNearUnits[i]) == false && !settingsPlugin.ignoredPlayers.Contains(tempNearUnits[i].Name))
                            {
                                if (tempNearUnits[i].DistanceToPlayer > settingsPlugin.scanMaxDistance)
                                {
                                    return;
                                }

                                if (settingsPlugin.scanOnlyTargettingPlayer)
                                {
                                    if (tempNearUnits[i].TargetGuid == ObjectManager.Instance.Player.Guid)
                                    {
                                        PlayAlert("ALERT! " + tempNearUnits[i].Name + " - " + tempNearUnits[i].Level.ToString(), settingsPlugin.alertType, settingsPlugin.playAudio, settingsPlugin.audioToPlay);
                                        ignoredPlayers.Add(tempNearUnits[i]);
                                    }
                                }
                                else
                                {
                                    PlayAlert("ALERT! " + tempNearUnits[i].Name + " - " + tempNearUnits[i].Level.ToString(), settingsPlugin.alertType, settingsPlugin.playAudio, settingsPlugin.audioToPlay);
                                    ignoredPlayers.Add(tempNearUnits[i]);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                //Helpers.PrintToChat("Error! " + exc.GetType().ToString());
            }
            
        }
        void clearIgnoredPlayers(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (clearIgnoreTimer.Interval != settingsPlugin.clearIgnoreAfterMS)
                {
                    clearIgnoreTimer.Interval = settingsPlugin.clearIgnoreAfterMS;
                }
                /*
                if (isStarted)
                {
                    Helpers.PrintToChat("Clearing Recent Players. Will clear again in " + (settingsPlugin.clearIgnoreAfterMS/1000).ToString() + " seconds.");
                }
                */
                ignoredPlayers.Clear();
                ignoredPlayers = null;
                ignoredPlayers = new List<WoWUnit>();
            }
            catch
            {

            }
        }
    }

}
