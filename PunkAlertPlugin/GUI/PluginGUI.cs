﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Settings;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using PunkAlertPlugin.MoreMethods;
using ZzukBot.Game.Statics;
using PunkAlertPlugin.Settings;

namespace PunkAlertPlugin.GUI
{
    public partial class PluginGUI : Form
    {
        public PunkAlertPlugin parentPluginObj;
        public PunkAlertSettings settingsGUI;

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public PluginGUI()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Helpers.PrintToChat("Closing GUI with no save");
            this.Close();
        }

        private void PluginGUI_Load(object sender, EventArgs e)
        {

        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void buttonPreviewAlert_Click(object sender, EventArgs e)
        {
            parentPluginObj.PlayAlert("Punk Alert Preview!!!!", comboBoxAlertType.SelectedIndex, checkBoxPlayAudio.Checked, comboBoxAudioFile.SelectedIndex);
        }

        private void linkLabelSeeSharp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=1675");
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            radioButton2.Checked = false;
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            radioButton1.Checked = false;
            radioButton2.Checked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/viewtopic.php?f=59&t=1153");
        }

        private void buttonSaveAndClose_Click(object sender, EventArgs e)
        {
            this.Close();
            Helpers.PrintToChat("Closing GUI with saving");
            SaveFormSettings();
            try
            {
                OptionManager.Get("PunkAlert").SaveToJson(parentPluginObj.settingsPlugin);
                Helpers.PrintToChat("Settings saved to JSON");
            }
            catch(Exception exc)
            {
                Helpers.PrintToChat("Error saving JSON! " + exc.GetType().ToString());
            }
        }

        public void LoadSettingsToForm()
        {
            try
            {
                if (parentPluginObj.settingsPlugin.alertStyle == 0)
                {
                    radioButton1.Checked = true;
                    radioButton2.Checked = false;
                }
                else if (parentPluginObj.settingsPlugin.alertStyle == 1)
                {
                    radioButton1.Checked = false;
                    radioButton2.Checked = true;
                }
                checkBox1.Checked = parentPluginObj.settingsPlugin.scanOnlyTargettingPlayer;
                numericUpDownMaxDistance.Value = parentPluginObj.settingsPlugin.scanMaxDistance;
                numericUpDownTickMS.Value = parentPluginObj.settingsPlugin.scanMSSpeed;
                checkBoxPlayAudio.Checked = parentPluginObj.settingsPlugin.playAudio;
                comboBoxAudioFile.SelectedIndex = parentPluginObj.settingsPlugin.audioToPlay;
                comboBoxAlertType.SelectedIndex = parentPluginObj.settingsPlugin.alertType;
                numericUpDownClearIgnoreTimer.Value = parentPluginObj.settingsPlugin.clearIgnoreAfterMS / 60 / 1000;
                Helpers.PrintToChat(parentPluginObj.settingsPlugin.ignoredPlayers.Count.ToString());
                if (parentPluginObj.settingsPlugin.ignoredPlayers.Count > 0)
                {
                    for (int i = 0; i < parentPluginObj.settingsPlugin.ignoredPlayers.Count; i += 1)
                    {
                        listBoxIgnoredPlayers.Items.Add(parentPluginObj.settingsPlugin.ignoredPlayers[i]);
                    }
                }
                Helpers.PrintToChat("Form Loaded from Settings!");
            }
            catch (Exception exc)
            {
                Helpers.PrintToChat("Error Loading! " + exc.GetType().ToString());
            }
        }

        public void SaveFormSettings()
        {
            try
            {
                if (radioButton1.Checked)
                {
                    parentPluginObj.settingsPlugin.alertStyle = 0;
                }
                else if (radioButton2.Checked)
                {
                    parentPluginObj.settingsPlugin.alertStyle = 1;
                }

                parentPluginObj.settingsPlugin.scanOnlyTargettingPlayer = checkBox1.Checked;
                parentPluginObj.settingsPlugin.scanMaxDistance = (int)numericUpDownMaxDistance.Value;
                parentPluginObj.settingsPlugin.scanMSSpeed = (int)numericUpDownTickMS.Value;
                parentPluginObj.settingsPlugin.playAudio = checkBoxPlayAudio.Checked;
                parentPluginObj.settingsPlugin.audioToPlay = comboBoxAudioFile.SelectedIndex;
                parentPluginObj.settingsPlugin.alertType = comboBoxAlertType.SelectedIndex;
                parentPluginObj.settingsPlugin.clearIgnoreAfterMS = (int)numericUpDownClearIgnoreTimer.Value * 60 * 1000;

                parentPluginObj.settingsPlugin.ignoredPlayers.Clear();

                if (listBoxIgnoredPlayers.Items.Count > 0)
                {
                    for (int i = 0; i < listBoxIgnoredPlayers.Items.Count; i += 1)
                    {
                        parentPluginObj.settingsPlugin.ignoredPlayers.Add(listBoxIgnoredPlayers.Items[i].ToString());
                    }
                }


                Helpers.PrintToChat("Settings Saved to Current Session!");
            }
            catch(Exception exc)
            {
                Helpers.PrintToChat("Error Saving! " + exc.GetType().ToString());
            }
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void buttonIgnorePlayer_Click(object sender, EventArgs e)
        {
            if (textBoxPlayerToIgnore.Text != "" && !listBoxIgnoredPlayers.Items.Contains(textBoxPlayerToIgnore.Text))
            {
                listBoxIgnoredPlayers.Items.Add(textBoxPlayerToIgnore.Text);
            }
        }

        private void buttonUnignoreSelected_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(listBoxIgnoredPlayers);
            selectedItems = listBoxIgnoredPlayers.SelectedItems;
            if (listBoxIgnoredPlayers.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    listBoxIgnoredPlayers.Items.Remove(selectedItems[i]);
                }
            }
        }

        private void buttonIgnoreWoWTarget_Click(object sender, EventArgs e)
        {
            if (ObjectManager.Instance.IsIngame && ObjectManager.Instance.Target != null && ObjectManager.Instance.Target.IsPlayer && !listBoxIgnoredPlayers.Items.Contains(ObjectManager.Instance.Target.Name))
            {
                listBoxIgnoredPlayers.Items.Add(ObjectManager.Instance.Target.Name);
            }
        }

        private void comboBoxAlertType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
