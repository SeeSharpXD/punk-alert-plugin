﻿namespace PunkAlertPlugin.GUI
{
    partial class PluginGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PluginGUI));
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxAlertType = new System.Windows.Forms.ComboBox();
            this.buttonSaveAndClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxPlayAudio = new System.Windows.Forms.CheckBox();
            this.comboBoxAudioFile = new System.Windows.Forms.ComboBox();
            this.buttonPreviewAlert = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxCloseNoSave = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownTickMS = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.numericUpDownMaxDistance = new System.Windows.Forms.NumericUpDown();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.linkLabelSeeSharp = new System.Windows.Forms.LinkLabel();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDownClearIgnoreTimer = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.radioButtonLogoutAfterAlerts = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBoxReconnectIfDC = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDownLogBackInAfter = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDownLogOutAfterGanked = new System.Windows.Forms.NumericUpDown();
            this.radioButtonLogoutAfterGanked = new System.Windows.Forms.RadioButton();
            this.radioButtonDontLogout = new System.Windows.Forms.RadioButton();
            this.listBoxIgnoredPlayers = new System.Windows.Forms.ListBox();
            this.textBoxPlayerToIgnore = new System.Windows.Forms.TextBox();
            this.buttonIgnorePlayer = new System.Windows.Forms.Button();
            this.buttonUnignoreSelected = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonIgnoreWoWTarget = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCloseNoSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTickMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxDistance)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownClearIgnoreTimer)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLogBackInAfter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLogOutAfterGanked)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(8, 408);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Alert Type: ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // comboBoxAlertType
            // 
            this.comboBoxAlertType.FormattingEnabled = true;
            this.comboBoxAlertType.Items.AddRange(new object[] {
            "Chat Message",
            "Dx9 Overlay (Coming Soon)"});
            this.comboBoxAlertType.Location = new System.Drawing.Point(93, 407);
            this.comboBoxAlertType.Name = "comboBoxAlertType";
            this.comboBoxAlertType.Size = new System.Drawing.Size(161, 21);
            this.comboBoxAlertType.TabIndex = 1;
            this.comboBoxAlertType.Text = "Default";
            this.comboBoxAlertType.SelectedIndexChanged += new System.EventHandler(this.comboBoxAlertType_SelectedIndexChanged);
            // 
            // buttonSaveAndClose
            // 
            this.buttonSaveAndClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveAndClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonSaveAndClose.Location = new System.Drawing.Point(320, 32);
            this.buttonSaveAndClose.Name = "buttonSaveAndClose";
            this.buttonSaveAndClose.Size = new System.Drawing.Size(243, 35);
            this.buttonSaveAndClose.TabIndex = 2;
            this.buttonSaveAndClose.Text = "Save and Close";
            this.buttonSaveAndClose.UseVisualStyleBackColor = true;
            this.buttonSaveAndClose.Click += new System.EventHandler(this.buttonSaveAndClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(28, 353);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Play Audio:";
            // 
            // checkBoxPlayAudio
            // 
            this.checkBoxPlayAudio.AutoSize = true;
            this.checkBoxPlayAudio.Location = new System.Drawing.Point(11, 355);
            this.checkBoxPlayAudio.Name = "checkBoxPlayAudio";
            this.checkBoxPlayAudio.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPlayAudio.TabIndex = 4;
            this.checkBoxPlayAudio.UseVisualStyleBackColor = true;
            // 
            // comboBoxAudioFile
            // 
            this.comboBoxAudioFile.FormattingEnabled = true;
            this.comboBoxAudioFile.Items.AddRange(new object[] {
            "PVP Flag Taken",
            "PVP Flag Return",
            "PVP Flag Capture"});
            this.comboBoxAudioFile.Location = new System.Drawing.Point(117, 352);
            this.comboBoxAudioFile.Name = "comboBoxAudioFile";
            this.comboBoxAudioFile.Size = new System.Drawing.Size(137, 21);
            this.comboBoxAudioFile.TabIndex = 5;
            this.comboBoxAudioFile.Text = "Default";
            // 
            // buttonPreviewAlert
            // 
            this.buttonPreviewAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPreviewAlert.ForeColor = System.Drawing.Color.Black;
            this.buttonPreviewAlert.Location = new System.Drawing.Point(320, 5);
            this.buttonPreviewAlert.Name = "buttonPreviewAlert";
            this.buttonPreviewAlert.Size = new System.Drawing.Size(243, 24);
            this.buttonPreviewAlert.TabIndex = 6;
            this.buttonPreviewAlert.Text = "Preview Alert";
            this.buttonPreviewAlert.UseVisualStyleBackColor = true;
            this.buttonPreviewAlert.Click += new System.EventHandler(this.buttonPreviewAlert_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label3.Location = new System.Drawing.Point(3, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "MS Between Scans:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.pictureBoxCloseNoSave);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(575, 29);
            this.panel1.TabIndex = 8;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // pictureBoxCloseNoSave
            // 
            this.pictureBoxCloseNoSave.Image = global::PunkAlertPlugin.Properties.Resources.x_red;
            this.pictureBoxCloseNoSave.Location = new System.Drawing.Point(545, 2);
            this.pictureBoxCloseNoSave.Name = "pictureBoxCloseNoSave";
            this.pictureBoxCloseNoSave.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxCloseNoSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCloseNoSave.TabIndex = 9;
            this.pictureBoxCloseNoSave.TabStop = false;
            this.pictureBoxCloseNoSave.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(3, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Punk Alert!";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(468, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 20);
            this.label10.TabIndex = 23;
            this.label10.Text = "V: 0.9.2";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // numericUpDownTickMS
            // 
            this.numericUpDownTickMS.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownTickMS.Location = new System.Drawing.Point(149, 82);
            this.numericUpDownTickMS.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownTickMS.Minimum = new decimal(new int[] {
            750,
            0,
            0,
            0});
            this.numericUpDownTickMS.Name = "numericUpDownTickMS";
            this.numericUpDownTickMS.Size = new System.Drawing.Size(82, 20);
            this.numericUpDownTickMS.TabIndex = 9;
            this.numericUpDownTickMS.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label5.Location = new System.Drawing.Point(25, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(206, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "^ Lower = Faster Alert. Higher = Less CPU";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(284, 79);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label6.Location = new System.Drawing.Point(3, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Max Distance:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBox1.Location = new System.Drawing.Point(6, 31);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(212, 20);
            this.checkBox1.TabIndex = 13;
            this.checkBox1.Text = "Only players targetting you";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // numericUpDownMaxDistance
            // 
            this.numericUpDownMaxDistance.Location = new System.Drawing.Point(112, 57);
            this.numericUpDownMaxDistance.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownMaxDistance.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownMaxDistance.Name = "numericUpDownMaxDistance";
            this.numericUpDownMaxDistance.Size = new System.Drawing.Size(103, 20);
            this.numericUpDownMaxDistance.TabIndex = 14;
            this.numericUpDownMaxDistance.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.radioButton1.Location = new System.Drawing.Point(12, 162);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(138, 17);
            this.radioButton1.TabIndex = 15;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Alert on PVP Attack";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.radioButton1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numericUpDownTickMS);
            this.groupBox1.Controls.Add(this.numericUpDownMaxDistance);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(6, 189);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 143);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.radioButton2.Location = new System.Drawing.Point(6, 0);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(141, 17);
            this.radioButton2.TabIndex = 18;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Alert on Player Scan";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Click += new System.EventHandler(this.radioButton2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(156, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "(someone attacks you)";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.buttonPreviewAlert);
            this.panel2.Controls.Add(this.buttonSaveAndClose);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Location = new System.Drawing.Point(0, 504);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(575, 86);
            this.panel2.TabIndex = 19;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(147, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(107, 27);
            this.button3.TabIndex = 9;
            this.button3.Text = "Reload";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.DimGray;
            this.button2.Location = new System.Drawing.Point(12, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 27);
            this.button2.TabIndex = 8;
            this.button2.Text = "Debug Panel (NYI)";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(12, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(242, 27);
            this.button1.TabIndex = 7;
            this.button1.Text = "Forum Thread";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label8.Location = new System.Drawing.Point(447, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "By: ";
            // 
            // linkLabelSeeSharp
            // 
            this.linkLabelSeeSharp.AutoSize = true;
            this.linkLabelSeeSharp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelSeeSharp.LinkColor = System.Drawing.Color.White;
            this.linkLabelSeeSharp.Location = new System.Drawing.Point(478, 35);
            this.linkLabelSeeSharp.Name = "linkLabelSeeSharp";
            this.linkLabelSeeSharp.Size = new System.Drawing.Size(89, 20);
            this.linkLabelSeeSharp.TabIndex = 21;
            this.linkLabelSeeSharp.TabStop = true;
            this.linkLabelSeeSharp.Text = "SeeSharp";
            this.linkLabelSeeSharp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSeeSharp_LinkClicked);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(194, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(169, 20);
            this.label9.TabIndex = 22;
            this.label9.Text = "________________";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label11.Location = new System.Drawing.Point(9, 471);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Clear Recent players every";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // numericUpDownClearIgnoreTimer
            // 
            this.numericUpDownClearIgnoreTimer.Location = new System.Drawing.Point(148, 469);
            this.numericUpDownClearIgnoreTimer.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownClearIgnoreTimer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownClearIgnoreTimer.Name = "numericUpDownClearIgnoreTimer";
            this.numericUpDownClearIgnoreTimer.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownClearIgnoreTimer.TabIndex = 19;
            this.numericUpDownClearIgnoreTimer.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label12.Location = new System.Drawing.Point(195, 471);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "minutes";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.numericUpDown1);
            this.groupBox2.Controls.Add(this.radioButtonLogoutAfterAlerts);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.checkBoxReconnectIfDC);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.numericUpDownLogBackInAfter);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.numericUpDownLogOutAfterGanked);
            this.groupBox2.Controls.Add(this.radioButtonLogoutAfterGanked);
            this.groupBox2.Controls.Add(this.radioButtonDontLogout);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox2.Location = new System.Drawing.Point(287, 159);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(273, 173);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "(Coming Next Version)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(173, 70);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 16);
            this.label17.TabIndex = 36;
            this.label17.Text = "alerts";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown1.Location = new System.Drawing.Point(123, 69);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(44, 20);
            this.numericUpDown1.TabIndex = 35;
            // 
            // radioButtonLogoutAfterAlerts
            // 
            this.radioButtonLogoutAfterAlerts.AutoSize = true;
            this.radioButtonLogoutAfterAlerts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLogoutAfterAlerts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.radioButtonLogoutAfterAlerts.Location = new System.Drawing.Point(21, 69);
            this.radioButtonLogoutAfterAlerts.Name = "radioButtonLogoutAfterAlerts";
            this.radioButtonLogoutAfterAlerts.Size = new System.Drawing.Size(98, 17);
            this.radioButtonLogoutAfterAlerts.TabIndex = 34;
            this.radioButtonLogoutAfterAlerts.Text = "Log out after";
            this.radioButtonLogoutAfterAlerts.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(36, 147);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 16);
            this.label16.TabIndex = 33;
            this.label16.Text = "(if disconnected)";
            // 
            // checkBoxReconnectIfDC
            // 
            this.checkBoxReconnectIfDC.AutoSize = true;
            this.checkBoxReconnectIfDC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxReconnectIfDC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxReconnectIfDC.Location = new System.Drawing.Point(21, 125);
            this.checkBoxReconnectIfDC.Name = "checkBoxReconnectIfDC";
            this.checkBoxReconnectIfDC.Size = new System.Drawing.Size(236, 20);
            this.checkBoxReconnectIfDC.TabIndex = 32;
            this.checkBoxReconnectIfDC.Text = "Re-connect with Zzukbot Login";
            this.checkBoxReconnectIfDC.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(212, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 16);
            this.label15.TabIndex = 31;
            this.label15.Text = "times";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label14.Location = new System.Drawing.Point(191, 99);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 16);
            this.label14.TabIndex = 30;
            this.label14.Text = "minutes";
            // 
            // numericUpDownLogBackInAfter
            // 
            this.numericUpDownLogBackInAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownLogBackInAfter.Location = new System.Drawing.Point(141, 99);
            this.numericUpDownLogBackInAfter.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownLogBackInAfter.Name = "numericUpDownLogBackInAfter";
            this.numericUpDownLogBackInAfter.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownLogBackInAfter.TabIndex = 29;
            this.numericUpDownLogBackInAfter.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownLogBackInAfter.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label13.Location = new System.Drawing.Point(18, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 16);
            this.label13.TabIndex = 28;
            this.label13.Text = "Log back in after";
            // 
            // numericUpDownLogOutAfterGanked
            // 
            this.numericUpDownLogOutAfterGanked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownLogOutAfterGanked.Location = new System.Drawing.Point(166, 44);
            this.numericUpDownLogOutAfterGanked.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownLogOutAfterGanked.Name = "numericUpDownLogOutAfterGanked";
            this.numericUpDownLogOutAfterGanked.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownLogOutAfterGanked.TabIndex = 27;
            // 
            // radioButtonLogoutAfterGanked
            // 
            this.radioButtonLogoutAfterGanked.AutoSize = true;
            this.radioButtonLogoutAfterGanked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLogoutAfterGanked.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.radioButtonLogoutAfterGanked.Location = new System.Drawing.Point(21, 44);
            this.radioButtonLogoutAfterGanked.Name = "radioButtonLogoutAfterGanked";
            this.radioButtonLogoutAfterGanked.Size = new System.Drawing.Size(146, 17);
            this.radioButtonLogoutAfterGanked.TabIndex = 17;
            this.radioButtonLogoutAfterGanked.Text = "Log out after Ganked";
            this.radioButtonLogoutAfterGanked.UseVisualStyleBackColor = true;
            // 
            // radioButtonDontLogout
            // 
            this.radioButtonDontLogout.AutoSize = true;
            this.radioButtonDontLogout.Checked = true;
            this.radioButtonDontLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDontLogout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.radioButtonDontLogout.Location = new System.Drawing.Point(21, 21);
            this.radioButtonDontLogout.Name = "radioButtonDontLogout";
            this.radioButtonDontLogout.Size = new System.Drawing.Size(95, 17);
            this.radioButtonDontLogout.TabIndex = 16;
            this.radioButtonDontLogout.TabStop = true;
            this.radioButtonDontLogout.Text = "Dont Logout";
            this.radioButtonDontLogout.UseVisualStyleBackColor = true;
            // 
            // listBoxIgnoredPlayers
            // 
            this.listBoxIgnoredPlayers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.listBoxIgnoredPlayers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxIgnoredPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxIgnoredPlayers.ForeColor = System.Drawing.Color.Yellow;
            this.listBoxIgnoredPlayers.FormattingEnabled = true;
            this.listBoxIgnoredPlayers.Location = new System.Drawing.Point(6, 21);
            this.listBoxIgnoredPlayers.Name = "listBoxIgnoredPlayers";
            this.listBoxIgnoredPlayers.Size = new System.Drawing.Size(128, 130);
            this.listBoxIgnoredPlayers.TabIndex = 27;
            // 
            // textBoxPlayerToIgnore
            // 
            this.textBoxPlayerToIgnore.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPlayerToIgnore.Location = new System.Drawing.Point(140, 24);
            this.textBoxPlayerToIgnore.Name = "textBoxPlayerToIgnore";
            this.textBoxPlayerToIgnore.Size = new System.Drawing.Size(126, 20);
            this.textBoxPlayerToIgnore.TabIndex = 28;
            // 
            // buttonIgnorePlayer
            // 
            this.buttonIgnorePlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIgnorePlayer.ForeColor = System.Drawing.Color.Black;
            this.buttonIgnorePlayer.Location = new System.Drawing.Point(140, 50);
            this.buttonIgnorePlayer.Name = "buttonIgnorePlayer";
            this.buttonIgnorePlayer.Size = new System.Drawing.Size(126, 23);
            this.buttonIgnorePlayer.TabIndex = 29;
            this.buttonIgnorePlayer.Text = "^ Ignore Player ^";
            this.buttonIgnorePlayer.UseVisualStyleBackColor = true;
            this.buttonIgnorePlayer.Click += new System.EventHandler(this.buttonIgnorePlayer_Click);
            // 
            // buttonUnignoreSelected
            // 
            this.buttonUnignoreSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUnignoreSelected.ForeColor = System.Drawing.Color.Black;
            this.buttonUnignoreSelected.Location = new System.Drawing.Point(140, 128);
            this.buttonUnignoreSelected.Name = "buttonUnignoreSelected";
            this.buttonUnignoreSelected.Size = new System.Drawing.Size(126, 23);
            this.buttonUnignoreSelected.TabIndex = 30;
            this.buttonUnignoreSelected.Text = "Un-ignore Selected";
            this.buttonUnignoreSelected.UseVisualStyleBackColor = true;
            this.buttonUnignoreSelected.Click += new System.EventHandler(this.buttonUnignoreSelected_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonIgnoreWoWTarget);
            this.groupBox3.Controls.Add(this.listBoxIgnoredPlayers);
            this.groupBox3.Controls.Add(this.buttonUnignoreSelected);
            this.groupBox3.Controls.Add(this.textBoxPlayerToIgnore);
            this.groupBox3.Controls.Add(this.buttonIgnorePlayer);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox3.Location = new System.Drawing.Point(288, 338);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(272, 160);
            this.groupBox3.TabIndex = 31;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ignored Players";
            // 
            // buttonIgnoreWoWTarget
            // 
            this.buttonIgnoreWoWTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIgnoreWoWTarget.ForeColor = System.Drawing.Color.Black;
            this.buttonIgnoreWoWTarget.Location = new System.Drawing.Point(140, 79);
            this.buttonIgnoreWoWTarget.Name = "buttonIgnoreWoWTarget";
            this.buttonIgnoreWoWTarget.Size = new System.Drawing.Size(126, 23);
            this.buttonIgnoreWoWTarget.TabIndex = 31;
            this.buttonIgnoreWoWTarget.Text = "Ignore WoW Target";
            this.buttonIgnoreWoWTarget.UseVisualStyleBackColor = true;
            this.buttonIgnoreWoWTarget.Click += new System.EventHandler(this.buttonIgnoreWoWTarget_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Gray;
            this.label19.Location = new System.Drawing.Point(12, 379);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(116, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Volume Comming Soon";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBox2.Location = new System.Drawing.Point(12, 434);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(189, 20);
            this.checkBox2.TabIndex = 35;
            this.checkBox2.Text = "Dont Alert on FlightPath";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // PluginGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.ClientSize = new System.Drawing.Size(574, 578);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.comboBoxAudioFile);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.linkLabelSeeSharp);
            this.Controls.Add(this.numericUpDownClearIgnoreTimer);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkBoxPlayAudio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxAlertType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox2);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PluginGUI";
            this.Text = "PluginGUI";
            this.Load += new System.EventHandler(this.PluginGUI_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCloseNoSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTickMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxDistance)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownClearIgnoreTimer)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLogBackInAfter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLogOutAfterGanked)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxAlertType;
        private System.Windows.Forms.Button buttonSaveAndClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxPlayAudio;
        private System.Windows.Forms.ComboBox comboBoxAudioFile;
        private System.Windows.Forms.Button buttonPreviewAlert;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBoxCloseNoSave;
        private System.Windows.Forms.NumericUpDown numericUpDownTickMS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxDistance;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel linkLabelSeeSharp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDownClearIgnoreTimer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericUpDownLogBackInAfter;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numericUpDownLogOutAfterGanked;
        private System.Windows.Forms.RadioButton radioButtonLogoutAfterGanked;
        private System.Windows.Forms.RadioButton radioButtonDontLogout;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBoxReconnectIfDC;
        private System.Windows.Forms.ListBox listBoxIgnoredPlayers;
        private System.Windows.Forms.TextBox textBoxPlayerToIgnore;
        private System.Windows.Forms.Button buttonIgnorePlayer;
        private System.Windows.Forms.Button buttonUnignoreSelected;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonIgnoreWoWTarget;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.RadioButton radioButtonLogoutAfterAlerts;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox checkBox2;
    }
}