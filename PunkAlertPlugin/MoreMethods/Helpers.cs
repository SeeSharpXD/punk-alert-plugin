﻿using ZzukBot.Game.Statics;
using ZzukBot.Objects;

namespace PunkAlertPlugin.MoreMethods
{
    internal static class Helpers
    {
        //2 - orc
        //4 - nightelf
        //5 - undead
        //message("text");
        public static void PrintToChat(string parMessage)
        {
            Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('PunkAlert: " + parMessage + "')");
        }
        public static void ShowMessage(string parMessage)
        {
            Lua.Instance.Execute("MessageFrame:AddMessage(" + parMessage + ", 1.0, 0.0, 0.0, 74, 5);");
        }
    }
}
