﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PunkAlertPlugin.Settings
{
    public class PunkAlertSettings
    {
        public int leet = 1337;
        public int alertStyle = 0;
        public bool scanOnlyTargettingPlayer = true;
        public int scanMaxDistance = 100;
        public int scanMSSpeed = 1000;
        public bool playAudio = true;
        public int audioToPlay = 0;
        public int alertType = 0;
        public int clearIgnoreAfterMS = 300000;
        public int logoutSetting = 0;
        public int logoutAfterGanked = 1;
        public int logoutAfterAlert = 3;
        public int loginAgainAfer = 5;
        public bool reconnectIfDC = true;
        public bool disableOnFP = true;

        public List<string> ignoredPlayers = new List<string>();
    }
}
